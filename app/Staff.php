<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Staff extends Model
{
    use \App\Concerns\UsesUuid;

    protected $table = 'staffs';

    protected $fillable = [
        'hotel_id',
        'user_id'
    ];

    public function getNameAttribute()
    {
        $this->user->name;
    }

    public function getEmailAttribute()
    {
        $this->user->email;
    }

    public function getAddressAttribute()
    {
        $this->user->address;
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function hotel()
    {
        return $this->belongsTo(Hotel::class);
    }
}
