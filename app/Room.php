<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    use \App\Concerns\UsesUuid;

    protected $table = 'rooms';

    protected $fillable = [
        'hotel_id',
        'name',
        'quantity'
    ];

    public function hotel()
    {
        return $this->belongsTo(Hotel::class);
    }
}
