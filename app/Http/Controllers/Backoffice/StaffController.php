<?php

namespace App\Http\Controllers\Backoffice;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DataTables;
use Auth;

use App\User;
use App\Staff;

class StaffController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            if (Auth::user()) {
              if (@Auth::user()->roles->first()->name == 'member') {
                return redirect('home');
              }
            }

            return $next($request);
        });
    }

    public function loadData()
    {
        $staffs = Staff::orderBy('created_at', 'desc');

        return DataTables::of($staffs)
              ->addIndexColumn()
              ->addColumn('name', function ($staff) {
                  return $staff->user->name;
              })
              ->addColumn('email', function ($staff) {
                  return $staff->user->email;
              })
              ->addColumn('address', function ($staff) {
                  return $staff->user->address;
              })
              ->addColumn('delete', function ($staff) {
                  if (Auth::user()->can('delete-hotel_staff')) {
                      return '<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-delete2" onclick="deleteModal2(\''.$staff->id.'\',\''.$staff->user->name.'\')">
                        <i class="fa fa-trash"></i>
                      </button>';
                  }
              })
              ->rawColumns(['delete'])
              ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($hotel_id)
    {
        $staff_ids = Staff::pluck('user_id')->toArray();
        $staffs = User::whereRoleIs('staff')
                      ->whereNotIn('id', $staff_ids)
                      ->pluck('name', 'id')
                      ->toArray();

        return view('backoffice.staffs.create', compact('hotel_id', 'staffs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        foreach ($request->staffs as $staff) {
          Staff::create([
            'user_id' => $staff,
            'hotel_id' => $request->hotel_id
          ]);
        }
        return redirect('hotels/'.$request->hotel_id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Staff $staff)
    {
        $hotel_id = $staff->hotel_id;
        $staff->delete();
        return redirect('hotels/'.$hotel_id);
    }
}
