<?php

namespace App\Http\Controllers\Backoffice;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DataTables;
use Auth;

use App\Hotel;
use App\Room;
use App\RoomOrder;

class RoomOrderController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            if (Auth::user()) {
              if (@Auth::user()->roles->first()->name == 'member') {
                return redirect('home');
              }
            }

            return $next($request);
        });
    }

    public function loadData()
    {
        $role = @Auth::user()->roles->first()->name;
        if ($role == 'admin') {
          $room_orders = RoomOrder::orderBy('created_at', 'desc');
        }else {
          if ($role == 'manager') {
            $hotels = Hotel::where('manager_id', Auth::user()->id)
                           ->pluck('id')
                           ->toArray();
          }else {
            $hotels = Staff::where('user_id', Auth::user()->id)
                           ->pluck('hotel_id')
                           ->toArray();
          }
          $rooms = Room::whereIn('hotel_id', $hotels)
                       ->pluck('id')
                       ->toArray();

          $room_orders = RoomOrder::whereIn('room_id', $rooms)
                                  ->orderBy('created_at', 'desc');
        }

        return DataTables::of($room_orders)
              ->addIndexColumn()
              ->addColumn('customer', function ($room_order) {
                  return $room_order->user->name;
              })
              ->addColumn('hotel', function ($room_order) {
                  return $room_order->room->hotel->name;
              })
              ->addColumn('room', function ($room_order) {
                  return $room_order->room->name;
              })
              ->editColumn('checkin', function ($room_order) {
                  return date('d M Y', strtotime($room_order->checkin));
              })
              ->editColumn('checkout', function ($room_order) {
                  return date('d M Y', strtotime($room_order->checkout));
              })
              ->addColumn('delete', function ($room_order) {
                  if (Auth::user()->can('delete-room_orders')) {
                    return '<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-delete" onclick="deleteModal(\''.$room_order->id.'\',\''.$room_order->name.'\')">
                      <i class="fa fa-trash"></i>
                    </button>';
                  }
              })
              ->addColumn('show', function ($room_order) {
                  $url = url('room_orders/'.$room_order->id);
                  return '<a href="'.$url.'" class="btn btn-primary">
                    <i class="fa fa-chevron-right"></i>
                  </a>';
              })
              ->rawColumns(['delete', 'show'])
              ->make(true);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backoffice.room_orders.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(RoomOrder $room_order)
    {
        return view('backoffice.room_orders.show', compact('room_order'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(RoomOrder $room_order)
    {
        return view('backoffice.room_orders.edit', compact('room_order'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RoomOrder $room_order)
    {
        $room_order->update($request->all());
        return redirect('room_orders');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(RoomOrder $room_order)
    {
        $room_order->delete();
        return redirect('room_orders');
    }
}
