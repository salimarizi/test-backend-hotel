<?php

namespace App\Http\Controllers\Backoffice;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DataTables;
use Auth;

use App\Room;

class RoomController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            if (Auth::user()) {
              if (@Auth::user()->roles->first()->name == 'member') {
                return redirect('home');
              }
            }

            return $next($request);
        });
    }

    public function loadData()
    {
        $rooms = Room::orderBy('created_at', 'desc');

        return DataTables::of($rooms)
              ->addIndexColumn()
              ->addColumn('edit', function ($room) {
                  if (Auth::user()->can('update-rooms')) {
                    $url = url('rooms/'.$room->id.'/edit');
                    return '<a href="'.$url.'" class="btn btn-success">
                      <i class="fa fa-edit"></i>
                    </a>';
                  }
              })
              ->addColumn('delete', function ($room) {
                  if (Auth::user()->can('delete-rooms')) {
                    return '<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-delete" onclick="deleteModal(\''.$room->id.'\',\''.$room->name.'\')">
                      <i class="fa fa-trash"></i>
                    </button>';
                  }
              })
              ->rawColumns(['edit', 'delete'])
              ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($hotel_id)
    {
        return view('backoffice.rooms.create', compact('hotel_id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Room::create($request->all());
        return redirect('hotels/'.$request->hotel_id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Room $room)
    {
        return view('backoffice.rooms.edit', compact('room'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Room $room)
    {
        $room->update($request->all());
        return redirect('hotels/'.$room->hotel_id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Room $room)
    {
        $hotel_id = $room->hotel_id;
        $room->delete();

        return redirect('hotels/'.$hotel_id);
    }
}
