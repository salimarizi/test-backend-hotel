<?php

namespace App\Http\Controllers\Backoffice;

use App\Http\Controllers\Controller;
use App\Http\Requests\HotelRequest;
use Illuminate\Http\Request;
use DataTables;
use Auth;

use App\Hotel;
use App\Room;
use App\User;
use App\Staff;

class HotelController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            if (Auth::user()) {
              if (@Auth::user()->roles->first()->name == 'member') {
                return redirect('home');
              }
            }

            return $next($request);
        });
    }

    public function loadData()
    {
        if (Auth::user()->roles->first()->name == 'admin') {
          $hotels = Hotel::orderBy('created_at', 'desc');
        }else if (Auth::user()->roles->first()->name == 'manager') {
          $hotels = Hotel::where('manager_id', Auth::user()->id)
                         ->orderBy('created_at', 'desc');
        }else {
          $hotel_ids = Staff::where('user_id', Auth::user()->id)
                            ->pluck('hotel_id')
                            ->toArray();

          $hotels = Hotel::whereIn('id', $hotel_ids)
                         ->orderBy('created_at', 'desc');
        }

        return DataTables::of($hotels)
              ->addIndexColumn()
              ->addColumn('manager', function ($hotel) {
                  return $hotel->manager->name;
              })
              ->addColumn('edit', function ($hotel) {
                  if (Auth::user()->can('update-hotels')) {
                    $url = url('hotels/'.$hotel->id.'/edit');
                    return '<a href="'.$url.'" class="btn btn-success">
                      <i class="fa fa-edit"></i>
                    </a>';
                  }
              })
              ->addColumn('delete', function ($hotel) {
                  if (Auth::user()->can('delete-hotels')) {
                    return '<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-delete" onclick="deleteModal(\''.$hotel->id.'\',\''.$hotel->name.'\')">
                      <i class="fa fa-trash"></i>
                    </button>';
                  }
              })
              ->addColumn('show', function ($hotel) {
                  if (Auth::user()->can('read-hotels')) {
                    $url = url('hotels/'.$hotel->id);
                    return '<a href="'.$url.'" class="btn btn-primary">
                      <i class="fa fa-chevron-right"></i>
                    </a>';
                  }
              })
              ->rawColumns(['edit', 'delete', 'show'])
              ->make(true);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backoffice.hotels.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $managers = User::whereRoleIs('manager')->pluck('name', 'id')->toArray();
        return view('backoffice.hotels.create', compact('managers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(HotelRequest $request)
    {
        $data = $request->all();
        if ($request->image) {
          $data['image'] = $this->uploadImage($request);
        }
        $hotel = Hotel::create($data);
        return redirect('hotels/'.$hotel->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Hotel $hotel)
    {
        return view('backoffice.hotels.show', compact('hotel'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Hotel $hotel)
    {
        $managers = User::whereRoleIs('manager')->pluck('name', 'id')->toArray();
        return view('backoffice.hotels.edit', compact('hotel', 'managers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(HotelRequest $request, Hotel $hotel)
    {
        $data = $request->all();

        if ($request->image) {
          $data['image'] = $this->uploadImage($request);
        }
        $hotel->update($data);
        return redirect('hotels/'.$hotel->id);
    }

    public function uploadImage(Request $request)
    {
        $imageName = time().'.'.request()->image->getClientOriginalExtension();
        request()->image->move(public_path('storage/hotels/'), $imageName);
        return $imageName;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Hotel $hotel)
    {
        $hotel->delete();
        return redirect('hotels');
    }
}
