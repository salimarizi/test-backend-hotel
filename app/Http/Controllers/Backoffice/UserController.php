<?php

namespace App\Http\Controllers\Backoffice;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use Illuminate\Http\Request;
use DataTables;
use Auth;

use App\User;
use App\Role;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            if (Auth::user()) {
              if (@Auth::user()->roles->first()->name == 'member') {
                return redirect('home');
              }
            }

            return $next($request);
        });
    }

    public function loadData()
    {
        $users = User::where('id', '!=', Auth::user()->id)
                     ->orderBy('created_at', 'desc');

        return DataTables::of($users)
              ->addIndexColumn()
              ->addColumn('role', function ($user) {
                return @$user->roles->first()->display_name;
              })
              ->addColumn('edit', function ($user) {
                  if (!$user->hasRole('admin')) {
                    $url = url('users/'.$user->id.'/edit');
                    return '<a href="'.$url.'" class="btn btn-success">
                      <i class="fa fa-edit"></i>
                    </a>';
                  }
              })
              ->addColumn('delete', function ($user) {
                  if (!$user->hasRole('admin')) {
                    return '<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-delete" onclick="deleteModal(\''.$user->id.'\',\''.$user->name.'\')">
                      <i class="fa fa-trash"></i>
                    </button>';
                  }
              })
              ->addColumn('show', function ($user) {
                  $url = url('users/'.$user->id);
                  return '<a href="'.$url.'" class="btn btn-primary">
                    <i class="fa fa-chevron-right"></i>
                  </a>';
              })
              ->rawColumns(['edit', 'delete', 'show'])
              ->make(true);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backoffice.users.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::pluck('display_name', 'name')->toArray();
        return view('backoffice.users.create', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        $data = $request->except('password_confirmation');
        $data['password'] = bcrypt($data['password']);

        $user = User::create($data);
        $user->attachRole($data['role']);

        return redirect('users');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return view('backoffice.users.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $roles = Role::pluck('display_name', 'name')->toArray();
        return view('backoffice.users.edit', compact('user', 'roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, User $user)
    {
        $data = $request->except('password_confirmation');
        if (!$data['password']) {
          unset($data['password']);
        }else {
          $data['password'] = bcrypt($data['password']);
        }

        $user->detachRoles($user->roles);
        $user->update($data);
        $user->attachRole($data['role']);

        return redirect('users');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->delete();
        return redirect('users');
    }
}
