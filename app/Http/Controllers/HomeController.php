<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

use App\User;
use App\Hotel;
use App\RoomOrder;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if (@Auth::user()->roles->first()->name == 'member' || @Auth::user()->roles->first()->name == null) {
          $hotels = Hotel::orderBy('created_at', 'desc')->get();
          return view('members.welcome', compact('hotels'));
        }
        return view('home');
    }

    public function my_orders()
    {
        $orders = RoomOrder::where('user_id', Auth::user()->id)->get();
        return view('members.my_orders', compact('orders'));
    }

    public function my_account()
    {
        if (@Auth::user()->roles->first()->name == 'member' || @Auth::user()->roles->first()->name == null) {
          return view('members.my_account');
        }
        return view('backoffice.profile');
    }

    public function update_profile(Request $request)
    {
        User::find(Auth::user()->id)->update($request->all());
        return redirect()->back();
    }

    public function cancel_order(RoomOrder $room_order)
    {
        $room_order->update([
          'status' => 'canceled'
        ]);

        return redirect()->back();
    }
}
