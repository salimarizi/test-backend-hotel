<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\RoomOrder;
use App\Http\Resources\RoomOrder as RoomOrderResource;
use App\Http\Resources\RoomOrderCollection;

class RoomOrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $sort_by = $request->sort_by;
        $order_by = $request->order_by;
        $search = $request->search;

        $room_orders = RoomOrder::orderBy($sort_by, $order_by);

        return new RoomOrderCollection($room_orders->paginate($request->per_page ? $request->per_page : 10));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $room_order = RoomOrder::create($request->all());
        return new RoomOrderResource($room_order);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(RoomOrder $room_order)
    {
        return new RoomOrderResource($room_order);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RoomOrder $room_order)
    {
        $room_order->update($request->all());
        return new RoomOrderResource(RoomOrder::find($room_order->id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function cancel(RoomOrder $room_order)
    {
        $room_order->update(['status' => 'canceled']);
        return response()->json(['message' => 'Order successfully canceled'], 200);
    }
}
