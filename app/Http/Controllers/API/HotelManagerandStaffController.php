<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Staff;
use App\Hotel;
use App\User;
use App\Http\Resources\Manager as ManagerResource;
use App\Http\Resources\ManagerCollection;
use App\Http\Resources\Staff as StaffResource;
use App\Http\Resources\StaffCollection;

class HotelManagerandStaffController extends Controller
{
    public function manager(Request $request)
    {
        $sort_by = $request->sort_by;
        $order_by = $request->order_by;

        $managers = User::whereRoleIs('manager')->orderBy($sort_by, $order_by);

        return (new ManagerCollection($managers->paginate($request->per_page ? $request->per_page : 10)));
    }

    public function staff(Request $request)
    {
        $sort_by = $request->sort_by;
        $order_by = $request->order_by;

        $managers = User::whereRoleIs('staff')->orderBy($sort_by, $order_by);

        return (new StaffCollection($managers->paginate($request->per_page ? $request->per_page : 10)));
    }

    public function staffInhotel(Request $request, Hotel $hotel)
    {
        $sort_by = $request->sort_by;
        $order_by = $request->order_by;

        $staff_ids = Staff::where('hotel_id', $hotel_id)->pluck('user_id')->toArray();
        $staffs = User::whereRoleIs('staff')
                        ->whereIn('id', $staff_ids)
                        ->orderBy($sort_by, $order_by);

        return (new StaffCollection($staffs->paginate($request->per_page ? $request->per_page : 10)));
    }

    public function assignStafftoHotel(Request $request, Hotel $hotel)
    {
        $hotel_id = $hotel->id;
        foreach ($request->user_ids as $user_id) {
            Staff::create(compact('hotel_id', 'user_id'));
        }
        $staffs = User::whereIn('id', $request->user_ids);

        return new StaffCollection($staffs->paginate($request->per_page ? $request->per_page : 10));
    }

    public function unAssignStafffromHotel(Request $request, Hotel $hotel)
    {
        Staff::whereIn('user_id', $request->user_ids)->delete();
        return response()->json(['message' => 'Successfully unassign staff from hotel'], 200);
    }
}
