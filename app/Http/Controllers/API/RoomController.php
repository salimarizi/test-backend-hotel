<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Hotel;
use App\Room;
use App\Http\Resources\Room as RoomResource;
use App\Http\Resources\RoomCollection;

class RoomController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Hotel $hotel)
    {
        $sort_by = $request->sort_by;
        $order_by = $request->order_by;
        $search = $request->search;

        $rooms = $hotel->rooms()->orderBy($sort_by, $order_by);

        if ($search) {
          $rooms = $rooms->where('name', 'like', '%'.$search.'%');
        }

        return (new RoomCollection($rooms->paginate($request->per_page ? $request->per_page : 10)))
                    ->hotel($hotel)
                    ->search($search);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Hotel $hotel)
    {
        $data = $request->all();
        $data['hotel_id'] = $hotel->id;
        $room = Room::create($data);
        
        return new RoomResource($room);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Hotel $hotel, Room $room)
    {
        return new RoomResource($room);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Hotel $hotel, Room $room)
    {
        $room->update($request->all());
        return new RoomResource(Room::find($room->id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Hotel $hotel, Room $room)
    {
        $room->delete();
        return response()->json(['message' => 'Room Deleted Successfully'], 200);
    }
}
