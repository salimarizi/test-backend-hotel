<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Hotel;
use App\Http\Resources\Hotel as HotelResource;
use App\Http\Resources\HotelCollection;

class HotelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $sort_by = $request->sort_by;
        $order_by = $request->order_by;
        $search = $request->search;

        $hotels = Hotel::orderBy($sort_by, $order_by);

        if ($search) {
          $hotels = $hotels->where('name', 'like', '%'.$search.'%');
        }

        return (new HotelCollection($hotels->paginate($request->per_page ? $request->per_page : 10)))->search($search);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $hotel = Hotel::create($request->all());
        return new HotelResource($hotel);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Hotel $hotel)
    {
        return new HotelResource($hotel);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Hotel $hotel)
    {
        $hotel->update($request->all());
        return new HotelResource(Hotel::find($hotel->id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Hotel $hotel)
    {
        $hotel->delete();
        return response()->json(['message' => 'Hotel Deleted Successfully'], 200);
    }
}
