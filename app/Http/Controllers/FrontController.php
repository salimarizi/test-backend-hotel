<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

use App\Hotel;
use App\Room;
use App\RoomOrder;

class FrontController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if (Auth::user()) {
              if (@Auth::user()->roles->first()->name != 'member') {
                return redirect('home');
              }
            }
            
            return $next($request);
        });
    }

    public function index()
    {
        $hotels = Hotel::orderBy('created_at', 'desc')->get();
        return view('index', compact('hotels'));
    }

    public function hotel_detail(Request $request, Hotel $hotel)
    {
        $checkin = $request->checkin;
        $checkout = $request->checkout;

        $rooms = Room::where('hotel_id', $hotel->id)->get();

        if (!empty($checkin) && !empty($checkout)) {
          foreach ($rooms as $room) {
            $orders = RoomOrder::where('room_id', $room->id)
                               ->where(function ($query) use ($checkin, $checkout){
                                 return $query->whereBetween('checkin', [$checkin, $checkout])
                                              ->orWhereBetween('checkout', [$checkin, $checkout]);
                               })
                               ->get();

            if (count($orders) < $room->quantity) {
              $room->status = 'Available';
            }else {
              $room->status = 'Fully-Booked';
            }
          }
        }else {
          foreach ($rooms as $room) {
            $room->status = 'Available';
          }
        }

        return view('detail', compact('hotel', 'checkin', 'checkout', 'rooms'));
    }

    public function confirmation(Request $request)
    {
        $data = $request->all();
        $data['room'] = Room::find($data['room_id']);

        if (Auth::guest()) {
          return redirect('login');
        }
        return view('confirm_page', $data);
    }

    public function book_room(Request $request)
    {
        RoomOrder::create($request->all());
        return redirect('home');
    }
}
