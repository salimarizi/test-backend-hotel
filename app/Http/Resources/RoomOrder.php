<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class RoomOrder extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $data = parent::toArray($request);
        $data['hotel'] = $this->room->hotel;
        $data['room'] = $this->room;
        $data['user'] = $this->user;
        unset($data['room']['hotel']);
        
        return $data;
    }
}
