<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class RoomCollection extends ResourceCollection
{
    protected $search, $hotel;

    public function search($value){
        $this->search = $value;
        return $this;
    }

    public function hotel($value){
        $this->hotel = $value;
        return $this;
    }
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $data = parent::toArray($request);
        $data['search'] = $this->search;
        $data['hotel'] = $this->hotel;
        return $data;
    }
}
