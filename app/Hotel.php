<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hotel extends Model
{
    use \App\Concerns\UsesUuid;

    protected $table = 'hotels';

    protected $fillable = [
        'name',
        'manager_id',
        'address',
        'description',
        'image'
    ];

    public function rooms()
    {
        return $this->hasMany(Room::class);
    }

    public function manager()
    {
        return $this->belongsTo(User::class, 'manager_id');
    }
}
