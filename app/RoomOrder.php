<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoomOrder extends Model
{
    use \App\Concerns\UsesUuid;

    protected $table = 'room_orders';

    protected $fillable = [
        'room_id',
        'user_id',
        'checkin',
        'checkout',
        'status'
    ];

    public function room()
    {
        return $this->belongsTo(Room::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
