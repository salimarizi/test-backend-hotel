<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('test', function () {
  return 'test for unit test';
});

Route::get('/', 'FrontController@index');
Route::get('hotel_detail/{hotel}', 'FrontController@hotel_detail');
Route::post('confirmation', 'FrontController@confirmation');
Route::post('book_room', 'FrontController@book_room');

Auth::routes();

Route::get('auth/redirect/{provider}', 'Auth\SocialController@redirect');
Route::get('callback/{provider}', 'Auth\SocialController@callback');

Route::get('home', 'HomeController@index')->name('home');
Route::get('my_orders', 'HomeController@my_orders')->name('my_orders');
Route::get('my_account', 'HomeController@my_account')->name('my_account');
Route::post('update_profile', 'HomeController@update_profile');
Route::delete('cancel_order/{room_order}', 'HomeController@cancel_order');


Route::namespace('Backoffice')->group(function () {
  Route::get('users/data', 'UserController@loadData');
  Route::resource('users', 'UserController');

  Route::get('room_orders/data', 'RoomOrderController@loadData');
  Route::resource('room_orders', 'RoomOrderController');

  Route::get('hotels/data', 'HotelController@loadData');
  Route::resource('hotels', 'HotelController');

  Route::get('rooms/create/{hotel_id}', 'RoomController@create');
  Route::get('rooms/data', 'RoomController@loadData');
  Route::resource('rooms', 'RoomController');

  Route::get('staffs/create/{staff_id}', 'StaffController@create');
  Route::get('staffs/data', 'StaffController@loadData');
  Route::resource('staffs', 'StaffController');
});
