<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::namespace('API')->group(function () {
  Route::post('register', 'AuthController@register');
  Route::post('login', 'AuthController@login');
  Route::get('refresh_token', 'AuthController@refreshToken');
});

Route::namespace('API')->middleware('jwt.verify')->group(function () {
  Route::get('profile', 'AuthController@getAuthenticatedUser');
  Route::post('profile', 'AuthController@updateProfile');

  Route::get('hotels/{hotel}/staffs', 'HotelManagerandStaffController@staffInHotel');
  Route::post('hotels/{hotel}/staffs/assign', 'HotelManagerandStaffController@assignStafftoHotel');
  Route::post('hotels/{hotel}/staffs/unassign', 'HotelManagerandStaffController@unAssignStafffromHotel');

  Route::resource('hotels/{hotel}/rooms', 'RoomController');
  Route::resource('hotels', 'HotelController');

  Route::resource('users', 'UserController');
  Route::post('room_orders/{room_order}/cancel', 'RoomOrderController@cancel');
  Route::resource('room_orders', 'RoomOrderController');

  Route::get('managers', 'HotelManagerandStaffController@manager');
  Route::get('staffs', 'HotelManagerandStaffController@staff');
});
