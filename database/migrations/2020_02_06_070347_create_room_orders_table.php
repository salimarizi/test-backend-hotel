<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRoomOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('room_orders', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('room_id');
            $table->string('user_id');
            $table->date('checkin');
            $table->date('checkout');
            $table->enum('status', ['booked', 'approved', 'declined', 'canceled'])->default('booked');
            $table->timestamps();

            $table->foreign('room_id')
                  ->references('id')
                  ->on('rooms')
                  ->onUpdate('cascade')
                  ->onDelete('cascade');

            $table->foreign('user_id')
                  ->references('id')
                  ->on('users')
                  ->onUpdate('cascade')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('room_orders');
    }
}
