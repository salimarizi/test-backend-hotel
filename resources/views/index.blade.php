@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        @foreach ($hotels as $hotel)
          <div class="col-md-4">
            <div class="card">
              <div class="card-header">{{ $hotel->name }}</div>

              <div class="card-body">
                <div class="row">
                  <div class="col-md-12">
                    @if ($hotel->image)
                      <img src="{!! asset('storage/hotels/'.$hotel->image) !!}" class="img img-fluid">
                    @else
                      <img src="{!! asset('default_images/hotel.jpg') !!}" class="img img-fluid">
                    @endif
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <a href="{{ url('hotel_detail/'.$hotel->id) }}"><h4 align="center">{{ $hotel->name }}</h4></a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        @endforeach
    </div>
</div>
@endsection
