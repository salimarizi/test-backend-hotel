<ul class="sidebar-menu">
  <li class="header">MENU</li>
  <!-- Optionally, you can add icons to the links -->
  <li>
    <a href="{{ url('/home') }}"><i class="fa fa-dashboard"></i>
      <span>Dashboard</span>
    </a>
  </li>

  <li>
    <a href="{{ url('room_orders') }}"><i class="fa fa-money"></i>
      <span>Orders</span>
    </a>
  </li>

  <li>
    <a href="{{ url('hotels') }}"><i class="fa fa-building"></i>
      <span>Hotels</span>
    </a>
  </li>

  @role('admin')
  <li>
    <a href="{{ url('users') }}"><i class="fa fa-users"></i>
      <span>Users</span>
    </a>
  </li>

  <li>
    <a href="{{ url('telescope') }}"><i class="fa fa-check"></i>
      <span>Audit Log</span>
    </a>
  </li>
  @endrole
</ul>
