@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">{{ $hotel->name }}</div>

                <div class="card-body">
                  <div class="row">
                    <div class="col-md-6">
                      <div class="col-md-12">
                        @if ($hotel->image)
                          <img src="{!! asset('storage/hotels/'.$hotel->image) !!}" class="img img-fluid">
                        @else
                          <img src="{!! asset('default_images/hotel.jpg') !!}" class="img img-fluid">
                        @endif
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="row">
                        <div class="col-md-4">Hotel Name</div>
                        <div class="col-md-8">: {{ $hotel->name }}</div>
                      </div>
                      <div class="row">
                        <div class="col-md-4">Address</div>
                        <div class="col-md-8">: {{ $hotel->address }}</div>
                      </div>
                      <div class="row">
                        <div class="col-md-4">Description</div>
                        <div class="col-md-8">: {{ $hotel->description }}</div>
                      </div>
                    </div>
                  </div>
                  <hr>
                  <div class="row">
                    <div class="col-md-12">
                      <h3>Choose your checkin and checkout</h3>
                    </div>
                  </div>
                  <form action="{{ url('hotel_detail/'.$hotel->id) }}" method="get">
                  <div class="row">
                      <div class="col-md-4">
                        <div class="form-group row">
                          <div class="col-md-4">
                            <label>Checkin</label>
                          </div>
                          <div class="col-md-8">
                            <input type="date" name="checkin" class="form-control" placeholder="{{ date('Y-m-d') }}">
                          </div>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group row">
                          <div class="col-md-4">
                            <label>Checkout</label>
                          </div>
                          <div class="col-md-8">
                            <input type="date" name="checkout" class="form-control" placeholder="{{ date('Y-m-d') }}">
                          </div>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <button type="submit" class="btn btn-success">Check</button>
                      </div>
                  </div>
                  </form>
                  <hr>
                  <div class="row">
                    <div class="col-md-5">
                      <table class="table table-responsive table-bordered table-striped">
                        <thead>
                          <tr>
                            <th>No.</th>
                            <th>Name</th>
                            <th>Status</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          @php($salim = 1)
                          @foreach ($rooms as $room)
                            <tr>
                              <td>{{ $salim++ }}</td>
                              <td>{{ $room->name }}</td>
                              <td>{{ $room->status }}</td>
                              <td>
                                @if ($checkin)
                                  @if ($room->status == 'Available')
                                    <form action="{{ url('confirmation') }}" method="post">
                                      @csrf
                                      <input type="hidden" name="room_id" value="{{ $room->id }}">
                                      <input type="hidden" name="checkin" value="{{ $checkin }}">
                                      <input type="hidden" name="checkout" value="{{ $checkout }}">
                                      <button type="submit" class="btn btn-primary">Book Now</button>
                                    </form>
                                  @else
                                    <a href="#" class="btn btn-danger" disabled>Full</a>
                                  @endif
                                @else
                                  Choose Date
                                @endif
                              </td>
                            </tr>
                          @endforeach
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection
