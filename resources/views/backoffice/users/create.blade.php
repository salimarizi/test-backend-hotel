@extends('layouts.backoffice_layout')

@section('css')
  <link rel="stylesheet" href="{!! asset('admin_layouts/plugins/select2/select2.min.css') !!}">
@endsection

@section('title')
  User
@endsection

@section('sub_title')
  Create User
@endsection

@section('content')
      <div class="row">
        <div class="col-md-12">
          <div class="box box-success">
              <div class="box-header">
                <div class="row">
                  <div class="col-md-10">
                    <h4>Form User</h4>
                  </div>
                  <div class="col-md-2"></div>
                </div>
              </div>
              <div class="box-body">
              <form action="{{ url('users') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="row">
                  <div class="col-md-2">
                    <label>Fullname: </label>
                  </div>
                  <div class="col-md-8">
                      <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                          <input type="text" class="form-control" name="name" value="{{ old('name') }}" placeholder="Fullname" required>
                          <small class="text-danger">{{ $errors->first('name') }}</small>
                      </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-2">
                    <label>Role: </label>
                  </div>
                  <div class="col-md-8">
                    <div class="form-group{{ $errors->has('role') ? ' has-error' : '' }}">
                      <select class="form-control select2" name="role" required>
                        <option selected disabled>Choose role</option>
                        @foreach ($roles as $key => $val)
                          @if (old('role') == $key)
                            <option value="{{ $key }}" selected>{{ $val }}</option>
                          @else
                            <option value="{{ $key }}">{{ $val }}</option>
                          @endif
                        @endforeach
                      </select>
                      <small class="text-danger">{{ $errors->first('role') }}</small>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-2">
                    <label>Address: </label>
                  </div>
                  <div class="col-md-8">
                      <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                          <textarea name="address" class="form-control" placeholder="Address">{{ old('address') }}</textarea>
                          <small class="text-danger">{{ $errors->first('address') }}</small>
                      </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-2">
                    <label>Email: </label>
                  </div>
                  <div class="col-md-8">
                      <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                          <input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email" required>
                          <small class="text-danger">{{ $errors->first('email') }}</small>
                      </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-2">
                    <label>Password: </label>
                  </div>
                  <div class="col-md-8">
                      <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                          <input type="password" class="form-control" name="password" placeholder="Password" required>
                          <small class="text-danger">{{ $errors->first('password') }}</small>
                      </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-2">
                    <label>Password Confirmation: </label>
                  </div>
                  <div class="col-md-8">
                      <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                          <input type="password" class="form-control" name="password_confirmation" placeholder="Password Confirmation" required>
                          <small class="text-danger">{{ $errors->first('password_confirmation') }}</small>
                      </div>
                  </div>
                </div>
              </div>

              <div class="box-footer">
                <button type="submit" class="btn btn-primary pull-right">Save</button>
                </form>
              </div>
          </div>
        </div>
      </div>
@endsection
