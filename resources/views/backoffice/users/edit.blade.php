@extends('layouts.backoffice_layout')

@section('css')
  <link rel="stylesheet" href="{!! asset('admin_layouts/plugins/select2/select2.min.css') !!}">
@endsection

@section('title')
  Home
@endsection

@section('sub_title')
  Edit User
@endsection

@section('content')
      <div class="row">
        <div class="col-md-12">
          <div class="box box-success">
              <div class="box-header">
                <div class="row">
                  <div class="col-md-10">
                    <h4>Edit User</h4>
                  </div>
                  <div class="col-md-2"></div>
                </div>
              </div>
              <div class="box-body">
              <form action="{{ url('users/'.$user->id) }}" method="post" enctype="multipart/form-data">
                @csrf
                {{ method_field('PATCH') }}
                <div class="row">
                  <div class="col-md-2">
                    <label>Fullname: </label>
                  </div>
                  <div class="col-md-8">
                      <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                          <input type="text" class="form-control" name="name" value="{{ old('name') ? old('name') : $user->name }}" placeholder="Fullname" required>
                          <small class="text-danger">{{ $errors->first('name') }}</small>
                      </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-2">
                    <label>Role: </label>
                  </div>
                  <div class="col-md-8">
                    <div class="form-group{{ $errors->has('role') ? ' has-error' : '' }}">
                      <select class="form-control select2" name="role" required>
                        <option selected disabled>Choose role</option>
                        @foreach ($roles as $key => $val)
                          @if (old('role') == $key || @$user->roles->first()->name == $key)
                            <option value="{{ $key }}" selected>{{ $val }}</option>
                          @else
                            <option value="{{ $key }}">{{ $val }}</option>
                          @endif
                        @endforeach
                      </select>
                      <small class="text-danger">{{ $errors->first('role') }}</small>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-2">
                    <label>Address: </label>
                  </div>
                  <div class="col-md-8">
                      <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                          <textarea name="address" class="form-control" placeholder="Address">{{ old('address') ? old('address') : $user->address }}</textarea>
                          <small class="text-danger">{{ $errors->first('address') }}</small>
                      </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-2">
                    <label>Email: </label>
                  </div>
                  <div class="col-md-8">
                      <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                          <input type="email" class="form-control" name="email" value="{{ old('email') ? old('email') : $user->email }}" placeholder="Email" required>
                          <small class="text-danger">{{ $errors->first('email') }}</small>
                      </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-2">
                    <label>Password: </label>
                  </div>
                  <div class="col-md-8">
                      <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                          <input type="password" class="form-control" name="password" placeholder="Password">
                          <small class="text-danger">{{ $errors->first('password') }}</small>
                      </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-2">
                    <label>Konfirmasi Password: </label>
                  </div>
                  <div class="col-md-8">
                      <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                          <input type="password" class="form-control" name="password_confirmation" placeholder="Konfirmasi Password">
                          <small class="text-danger">{{ $errors->first('password_confirmation') }}</small>
                      </div>
                  </div>
                </div>

                {{-- <div class="row">
                  <div class="col-md-2">
                    <label>Gambar: </label>
                  </div>
                  <div class="col-md-8">
                      <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                          <input type="file" class="form-control" name="image">
                          <small class="text-danger">{{ $errors->first('image') }}</small>
                      </div>
                  </div>
                </div> --}}
              </div>

              <div class="box-footer">
                <button type="submit" class="btn btn-success pull-right">Edit</button>
                </form>
              </div>
          </div>
        </div>
      </div>
@endsection
