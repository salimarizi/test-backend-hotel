@extends('layouts.backoffice_layout')

@section('title')
  Home
@endsection

@section('sub_title')
  Detail User
@endsection

@section('content')
      <div class="row">
        <div class="col-md-12">
          <div class="box box-success">
              <div class="box-header">
                <div class="row">
                  <div class="col-md-10">
                    <h4>Detail User</h4>
                  </div>
                  <div class="col-md-2"></div>
                </div>
              </div>
              <div class="box-body">
                <div class="row">
                  <div class="col-md-4">
                    @if ($user->image)
                      <img src="{{ url('storage/users/'.$user->image) }}" class="img img-responsive">
                    @else
                      <img src="{{ url('default_images/user.png') }}" class="img img-responsive">
                    @endif
                  </div>
                  <div class="col-md-8">
                    <div class="row">
                      <div class="col-md-3">
                        <label>Fullname </label>
                      </div>
                      <div class="col-md-7">
                          : {{ $user->name }}
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-3">
                        <label>Role </label>
                      </div>
                      <div class="col-md-7">
                          : {{ @$user->roles->first()->display_name }}
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-3">
                        <label>Email </label>
                      </div>
                      <div class="col-md-7">
                          : {{ $user->email }}
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-3">
                        <label>Address </label>
                      </div>
                      <div class="col-md-7">
                        : {{ $user->address }}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
          </div>
        </div>
      </div>
@endsection
