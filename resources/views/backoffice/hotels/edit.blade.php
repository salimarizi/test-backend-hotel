@extends('layouts.backoffice_layout')

@section('css')
  <link rel="stylesheet" href="{!! asset('admin_layouts/plugins/select2/select2.min.css') !!}">
@endsection

@section('title')
  Home
@endsection

@section('sub_title')
  Edit Hotel
@endsection

@section('content')
      <div class="row">
        <div class="col-md-12">
          <div class="box box-success">
              <div class="box-header">
                <div class="row">
                  <div class="col-md-10">
                    <h4>Edit Hotel</h4>
                  </div>
                  <div class="col-md-2"></div>
                </div>
              </div>
              <div class="box-body">
              <form action="{{ url('hotels/'.$hotel->id) }}" method="post" enctype="multipart/form-data">
                @csrf
                {{ method_field('PATCH') }}
                <div class="row">
                  <div class="col-md-2">
                    <label>Hotel Name: </label>
                  </div>
                  <div class="col-md-8">
                      <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                          <input type="text" class="form-control" name="name" value="{{ old('name') ? old('name') : $hotel->name }}" placeholder="Fullname" required>
                          <small class="text-danger">{{ $errors->first('name') }}</small>
                      </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-2">
                    <label>Manager: </label>
                  </div>
                  <div class="col-md-8">
                    <div class="form-group{{ $errors->has('manager_id') ? ' has-error' : '' }}">
                      <select class="form-control select2" name="manager_id" required>
                        <option selected disabled>Choose Manager</option>
                        @foreach ($managers as $key => $val)
                          @if (old('manager_id') == $key || $hotel->manager_id == $key)
                            <option value="{{ $key }}" selected>{{ $val }}</option>
                          @else
                            <option value="{{ $key }}">{{ $val }}</option>
                          @endif
                        @endforeach
                      </select>
                      <small class="text-danger">{{ $errors->first('manager_id') }}</small>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-2">
                    <label>Description: </label>
                  </div>
                  <div class="col-md-8">
                      <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                          <textarea name="description" class="form-control" placeholder="Description">{{ old('description') ? old('description') : $hotel->description }}</textarea>
                          <small class="text-danger">{{ $errors->first('description') }}</small>
                      </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-2">
                    <label>Address: </label>
                  </div>
                  <div class="col-md-8">
                      <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                          <textarea name="address" class="form-control" placeholder="Address">{{ old('address') ? old('address') : $hotel->address }}</textarea>
                          <small class="text-danger">{{ $errors->first('address') }}</small>
                      </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-2">
                    <label>Image: </label>
                  </div>
                  <div class="col-md-8">
                      <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                          <input type="file" class="form-control" name="image">
                          <small class="text-danger">{{ $errors->first('image') }}</small>
                      </div>
                  </div>
                </div>
              </div>

              <div class="box-footer">
                <button type="submit" class="btn btn-success pull-right">Edit</button>
                </form>
              </div>
          </div>
        </div>
      </div>
@endsection
