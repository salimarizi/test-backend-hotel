@extends('layouts.backoffice_layout')

@section('title')
  Home
@endsection

@section('sub_title')
  Detail Hotel
@endsection

@section('modal')
  <div class="modal" id="modal-delete">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Delete Room</h4>
        </div>
        <div class="modal-body">
          <form action="{{ url('rooms') }}" method="post" id="form-delete">
            @csrf
            {{ method_field('DELETE') }}
            Are you sure want to delete room :
            <b><span id="room_name"></span></b>?
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">
              Close
            </button>
            <button type="submit" class="btn btn-danger">
              Delete
            </button>
          </form>
        </div>
      </div>
    </div>
  </div>

  <div class="modal" id="modal-delete2">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Delete Staff</h4>
        </div>
        <div class="modal-body">
          <form action="{{ url('staffs') }}" method="post" id="form-delete">
            @csrf
            {{ method_field('DELETE') }}
            Are you sure want to delete staff :
            <b><span id="staff_name"></span></b>?
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">
              Close
            </button>
            <button type="submit" class="btn btn-danger">
              Delete
            </button>
          </form>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('content')
      <div class="row">
        <div class="col-md-12">
          <div class="box box-success">
              <div class="box-header">
                <div class="row">
                  <div class="col-md-10">
                    <h4>Detail Hotel</h4>
                  </div>
                  <div class="col-md-2"></div>
                </div>
              </div>
              <div class="box-body">
                <div class="row">
                  <div class="col-md-4">
                    @if ($hotel->image)
                      <img src="{!! asset('storage/hotels/'.$hotel->image) !!}" class="img img-fluid img-responsive">
                    @else
                      <img src="{!! asset('default_images/hotel.jpg') !!}" class="img img-fluid img-responsive">
                    @endif
                  </div>
                  <div class="col-md-8">
                    <div class="row">
                      <div class="col-md-3">
                        <label>Hotel Name </label>
                      </div>
                      <div class="col-md-7">
                          : {{ $hotel->name }}
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-3">
                        <label>Manager </label>
                      </div>
                      <div class="col-md-7">
                          : {{ @$hotel->manager->name }}
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-3">
                        <label>Description </label>
                      </div>
                      <div class="col-md-7">
                          : {{ $hotel->description }}
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-3">
                        <label>Address </label>
                      </div>
                      <div class="col-md-7">
                        : {{ $hotel->address }}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-md-12">
          <div class="box box-success">
              <div class="box-header">
                <div class="row">
                  <div class="col-md-10">
                    <h4>Lists of Rooms</h4>
                  </div>
                  <div class="col-md-2">
                    @if (Auth::user()->can('create-rooms'))
                      <a href="{{ url('rooms/create/'.$hotel->id) }}" class="btn btn-primary">
                        <i class="fa fa-plus"></i> Add Room
                      </a>
                    @endif
                  </div>
                </div>
              </div>
              <div class="box-body">
                <table id="table" class="table table-bordered table-striped table-hover">
                  <thead>
                    <tr>
                      <th>No.</th>
                      <th>Name</th>
                      <th>Quantity</th>
                      <th></th>
                      <th></th>
                    </tr>
                  </thead>
                </table>
              </div>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-md-12">
          <div class="box box-success">
              <div class="box-header">
                <div class="row">
                  <div class="col-md-10">
                    <h4>Lists of Staff</h4>
                  </div>
                  <div class="col-md-2">
                    @if (Auth::user()->can('create-hotel_staff'))
                      <a href="{{ url('staffs/create/'.$hotel->id) }}" class="btn btn-primary">
                        <i class="fa fa-plus"></i> Add Staff
                      </a>
                    @endif
                  </div>
                </div>
              </div>
              <div class="box-body">
                <table id="table2" class="table table-bordered table-striped table-hover">
                  <thead>
                    <tr>
                      <th>No.</th>
                      <th>Name</th>
                      <th>Email</th>
                      <th>Address</th>
                      <th></th>
                    </tr>
                  </thead>
                </table>
              </div>
          </div>
        </div>
      </div>
@endsection

@section('js')
  <script type="text/javascript">
    $(function() {
        $('#table').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ url('rooms/data') }}",
            columns: [
                { data: 'DT_RowIndex', orderable: false, searchable: false },
                { data: 'name', name: 'name' },
                { data: 'quantity', name: 'quantity' },
                { data: 'edit', name: 'edit', orderable: false, searchable: false },
                { data: 'delete', name: 'delete', orderable: false, searchable: false }
            ]
        })
    })

    deleteModal = (id, name) => {
      $('#modal-delete #room_name').text(name)
      $('#modal-delete #form-delete').attr('action', "{{ url('rooms') }}/" + id)
    }

    $(function() {
        $('#table2').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ url('staffs/data') }}",
            columns: [
                { data: 'DT_RowIndex', orderable: false, searchable: false },
                { data: 'name', name: 'name' },
                { data: 'email', name: 'email' },
                { data: 'address', name: 'address' },
                { data: 'delete', name: 'delete', orderable: false, searchable: false }
            ]
        })
    })

    deleteModal2 = (id, name) => {
      $('#modal-delete2 #staff_name').text(name)
      $('#modal-delete2 #form-delete').attr('action', "{{ url('staffs') }}/" + id)
    }
  </script>
@endsection
