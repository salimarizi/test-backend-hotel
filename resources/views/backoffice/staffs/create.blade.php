@extends('layouts.backoffice_layout')

@section('css')
  <link rel="stylesheet" href="{!! asset('admin_layouts/plugins/select2/select2.min.css') !!}">
@endsection

@section('title')
  Staff
@endsection

@section('sub_title')
  Create Staff
@endsection

@section('content')
      <div class="row">
        <div class="col-md-12">
          <div class="box box-success">
              <div class="box-header">
                <div class="row">
                  <div class="col-md-10">
                    <h4>Form Staff</h4>
                  </div>
                  <div class="col-md-2"></div>
                </div>
              </div>
              <div class="box-body">
              <form action="{{ url('staffs') }}" method="post" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="hotel_id" value="{{ $hotel_id }}">

                <div class="row">
                  <div class="col-md-2">
                    <label>Staff Name: </label>
                  </div>
                  <div class="col-md-8">
                      <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                          <select class="form-control select2" id="staffs" name="staffs[]" multiple="multiple" required>
                            @foreach ($staffs as $key => $val)
                              <option value="{{ $key }}">{{ $val }}</option>
                            @endforeach
                          </select>
                          <small class="text-danger">{{ $errors->first('name') }}</small>
                      </div>
                  </div>
                </div>
              </div>

              <div class="box-footer">
                <button type="submit" class="btn btn-primary pull-right">Save</button>
                </form>
              </div>
          </div>
        </div>
      </div>
@endsection

@section('js')
  <script src="{!! asset('admin_layouts/plugins/select2/select2.full.min.js') !!}"></script>
  <script type="text/javascript">
    $("#staffs").select2({
      placeholder : 'Choose Staff'
    });
  </script>
@endsection
