@extends('layouts.backoffice_layout')

@section('css')
  <link rel="stylesheet" href="{!! asset('admin_layouts/plugins/select2/select2.min.css') !!}">
@endsection

@section('title')
  Room
@endsection

@section('sub_title')
  Create Room
@endsection

@section('content')
      <div class="row">
        <div class="col-md-12">
          <div class="box box-success">
              <div class="box-header">
                <div class="row">
                  <div class="col-md-10">
                    <h4>Form Room</h4>
                  </div>
                  <div class="col-md-2"></div>
                </div>
              </div>
              <div class="box-body">
              <form action="{{ url('rooms') }}" method="post" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="hotel_id" value="{{ $hotel_id }}">

                <div class="row">
                  <div class="col-md-2">
                    <label>Room Name: </label>
                  </div>
                  <div class="col-md-8">
                      <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                          <input type="text" class="form-control" name="name" value="{{ old('name') }}" placeholder="Room Name" required>
                          <small class="text-danger">{{ $errors->first('name') }}</small>
                      </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-2">
                    <label>Quantity: </label>
                  </div>
                  <div class="col-md-8">
                      <div class="form-group{{ $errors->has('quantity') ? ' has-error' : '' }}">
                          <input type="number" class="form-control" name="quantity" value="{{ old('quantity') }}" placeholder="Room Name" required>
                          <small class="text-danger">{{ $errors->first('quantity') }}</small>
                      </div>
                  </div>
                </div>
              </div>

              <div class="box-footer">
                <button type="submit" class="btn btn-primary pull-right">Save</button>
                </form>
              </div>
          </div>
        </div>
      </div>
@endsection
