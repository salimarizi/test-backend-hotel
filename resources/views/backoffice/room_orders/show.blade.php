@extends('layouts.backoffice_layout')

@section('title')
  Home
@endsection

@section('sub_title')
  Detail Order
@endsection

@section('content')
      <div class="row">
        <div class="col-md-12">
          <div class="box box-success">
              <div class="box-header">
                <div class="row">
                  <div class="col-md-10">
                    <h4>Detail Order</h4>
                  </div>
                  <div class="col-md-2"></div>
                </div>
              </div>
              <div class="box-body">
                <div class="row">
                  <div class="col-md-8">
                    <div class="row">
                      <div class="col-md-3">
                        <label>Customer Name </label>
                      </div>
                      <div class="col-md-7">
                          : {{ $room_order->user->name }}
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-3">
                        <label>Customer Email </label>
                      </div>
                      <div class="col-md-7">
                          : {{ $room_order->user->email }}
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-3">
                        <label>Hotel Name</label>
                      </div>
                      <div class="col-md-7">
                          : {{ $room_order->room->hotel->name }}
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-3">
                        <label>Hotel Room</label>
                      </div>
                      <div class="col-md-7">
                          : {{ $room_order->room->name }}
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-3">
                        <label>Checkin</label>
                      </div>
                      <div class="col-md-7">
                          : {{ date('d M Y', strtotime($room_order->checkin)) }}
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-3">
                        <label>Checkout</label>
                      </div>
                      <div class="col-md-7">
                          : {{ date('d M Y', strtotime($room_order->checkout)) }}
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-3">
                        <label>Status</label>
                      </div>
                      <div class="col-md-7">
                          : {{ ucfirst($room_order->status) }}
                      </div>
                    </div>
                  </div>
                </div>
                <hr>
                <div class="row">

                    @if (Auth::user()->can('update-room_orders'))
                      @if ($room_order->status == 'booked')
                        <div class="col-md-1">
                          <form action="{{ url('room_orders/'.$room_order->id) }}" method="post">
                            @csrf
                            {{ method_field("PATCH") }}
                            <input type="hidden" name="status" value="approved">
                            <button type="submit" class="btn btn-primary">
                              Approve
                            </button>
                          </form>
                        </div>
                        <div class="col-md-2">
                          <form action="{{ url('room_orders/'.$room_order->id) }}" method="post">
                            @csrf
                            {{ method_field("PATCH") }}
                            <input type="hidden" name="status" value="declined">
                            <button type="submit" class="btn btn-danger">
                              Declined
                            </button>
                          </form>
                        </div>
                      @endif
                    @endif
                </div>
              </div>
          </div>
        </div>
      </div>
@endsection
