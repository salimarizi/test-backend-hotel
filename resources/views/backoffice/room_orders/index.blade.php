@extends('layouts.backoffice_layout')

@section('title')
  Orders
@endsection

@section('sub_title')
  List Orders
@endsection

@section('modal')
  <div class="modal" id="modal-delete">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Delete Orders</h4>
        </div>
        <div class="modal-body">
          <form action="{{ url('room_orders') }}" method="post" id="form-delete">
            @csrf
            {{ method_field('DELETE') }}
            Are you sure want to delete this order
            <b><span id="hotel_name"></span></b>?
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">
              Close
            </button>
            <button type="submit" class="btn btn-danger">
              Delete
            </button>
          </form>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('content')
      <div class="row">
        <div class="col-md-12">
          <div class="box box-success">
              <div class="box-header">
                <div class="row">
                  <div class="col-md-10">
                    <h4>Lists of Orders</h4>
                  </div>
                </div>
              </div>
              <div class="box-body">
                <table id="table" class="table table-bordered table-striped table-hover">
                  <thead>
                    <tr>
                      <th>No.</th>
                      <th>Customer</th>
                      <th>Hotel</th>
                      <th>Room</th>
                      <th>Checkin</th>
                      <th>Checkout</th>
                      <th></th>
                      <th></th>
                    </tr>
                  </thead>
                </table>
              </div>
          </div>
        </div>
      </div>
@endsection

@section('js')
  <script type="text/javascript">
    $(function() {
        $('#table').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ url('room_orders/data') }}",
            columns: [
                { data: 'DT_RowIndex', orderable: false, searchable: false },
                { data: 'customer', name: 'customer' },
                { data: 'hotel', name: 'hotel' },
                { data: 'room', name: 'room' },
                { data: 'checkin', name: 'checkin' },
                { data: 'checkout', name: 'checkout' },
                { data: 'delete', name: 'delete', orderable: false, searchable: false },
                { data: 'show', name: 'show', orderable: false, searchable: false },
            ]
        })
    })

    deleteModal = (id, name) => {
      $('#modal-delete #hotel_name').text(name)
      $('#modal-delete #form-delete').attr('action', "{{ url('hotels') }}/" + id)
    }
  </script>
@endsection
