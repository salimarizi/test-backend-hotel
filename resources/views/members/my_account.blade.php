@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
          <div class="card">
            <div class="card-header">
              <h4 class="card-title">My Account</h4>
            </div>
            <form action="{{ url('update_profile') }}" method="post">
            @csrf
            <div class="card-body">
              <div class="row">
                <div class="col-md-4">
                  <label class="label label-default">Name</label>
                </div>
                <div class="col-md-8">
                  <input type="text" name="name" class="form-control" placeholder="Name" value="{{ Auth::user()->name }}">
                </div>
              </div>
              <div class="row">
                <div class="col-md-4">
                  <label class="label label-default">Address</label>
                </div>
                <div class="col-md-8">
                  <textarea name="address" class="form-control" placeholder="Address">{{ Auth::user()->address }}</textarea>
                </div>
              </div>
            </div>

            <div class="card-footer" style="text-align:right">
              <button type="submit" class="btn btn-success">
                Submit
              </button>
            </div>
            </form>
          </div>
        </div>
    </div>
</div>
@endsection
