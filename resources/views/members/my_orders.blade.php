@extends('layouts.app')

@section('modal')
  <div class="modal" id="modal-delete">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          Cancel Orders
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
        </div>
        <div class="modal-body">
          <form action="{{ url('room_orders') }}" method="post" id="form-delete">
            @csrf
            {{ method_field('DELETE') }}
            Are you sure want to delete this order
            <b><span id="hotel_name"></span></b>?
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">
              Close
            </button>
            <button type="submit" class="btn btn-danger">
              Cancel
            </button>
          </form>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    <table id="table" class="table table-bordered table-striped table-hover">
                      <thead>
                        <tr>
                          <th>No.</th>
                          <th>Hotel</th>
                          <th>Room</th>
                          <th>Checkin</th>
                          <th>Checkout</th>
                          <th>Status</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                        @php($salim=1)
                        @foreach ($orders as $order)
                          <tr>
                            <td>{{ $salim++ }}</td>
                            <td>{{ $order->room->hotel->name }}</td>
                            <td>{{ $order->room->name }}</td>
                            <td>{{ date('d M Y', strtotime($order->checkin)) }}</td>
                            <td>{{ date('d M Y', strtotime($order->checkout)) }}</td>
                            <td>{{ ucfirst($order->status) }}</td>
                            <td>
                              @if ($order->status == 'booked')
                                <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-delete" onclick="deleteModal('{{ $order->id }}')">
                                  x
                                </button>
                              @endif
                            </td>
                          </tr>
                        @endforeach
                      </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
  <script type="text/javascript">
  deleteModal = (id) => {
    $('#modal-delete #form-delete').attr('action', "{{ url('cancel_order') }}/" + id)
  }
  </script>
@endsection
