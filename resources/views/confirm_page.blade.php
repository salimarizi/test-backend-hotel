@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-4">
            <div class="card">
                <div class="card-header"><h2>Order Review</h2></div>

                <div class="card-body">
                  <table class="table table-bordered table-striped">
                    <tr>
                      <td>Name</td>
                      <td>{{ Auth::user()->name }}</td>
                    </tr>
                    <tr>
                      <td>Email</td>
                      <td>{{ Auth::user()->email }}</td>
                    </tr>
                    <tr>
                      <td>Hotel</td>
                      <td>{{ $room->hotel->name }}</td>
                    </tr>
                    <tr>
                      <td>Room</td>
                      <td>{{ $room->name }}</td>
                    </tr>
                    <tr>
                      <td>Checkin</td>
                      <td>{{ date('d M Y', strtotime($checkin)) }}</td>
                    </tr>
                    <tr>
                      <td>Checkout</td>
                      <td>{{ date('d M Y', strtotime($checkout)) }}</td>
                    </tr>
                  </table>
                </div>

                <div class="card-footer">
                  <form action="{{ url('book_room') }}" method="post">
                    @csrf
                    <input type="hidden" name="room_id" value="{{ $room->id }}">
                    <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
                    <input type="hidden" name="checkin" value="{{ $checkin }}">
                    <input type="hidden" name="checkout" value="{{ $checkout }}">
                    <center><button type="submit" class="btn btn-primary">Submit Order</button></center>
                  </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
