@extends('layouts.backoffice_layout')

@section('title')
  Home
@endsection

@section('sub_title')
  Home
@endsection

@section('content')
    <h1 align="center">Welcome, {{ Auth::user()->name }}</h1>
@endsection
