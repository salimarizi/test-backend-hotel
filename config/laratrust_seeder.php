<?php

return [
    'role_structure' => [
        'admin' => [
            'users' => 'c,r,u,d',
            'hotels' => 'c,r,u,d',
            'rooms' => 'c,r,u,d',
            'room_orders' => 'c,r,u,d',
            'hotel_managers' => 'c,r,u,d',
            'hotel_staff' => 'c,r,u,d',
            'profile' => 'r,u'
        ],
        'manager' => [
            'hotels' => 'r,u',
            'rooms' => 'r,u,d',
            'room_orders' => 'r,u,d',
            'hotel_staff' => 'c,r,u,d',
            'profile' => 'r,u'
        ],
        'staff' => [
            'hotels' => 'r',
            'rooms' => 'r',
            'room_orders' => 'r,u',
            'hotel_staff' => 'r',
            'profile' => 'r,u'
        ],
        'member' => [
            'hotels' => 'r',
            'rooms' => 'r',
            'room_orders' => 'c,r,u',
            'profile' => 'r,u'
        ],
    ],
    'permission_structure' => [
        'cru_user' => [
            'profile' => 'c,r,u'
        ],
    ],
    'permissions_map' => [
        'c' => 'create',
        'r' => 'read',
        'u' => 'update',
        'd' => 'delete'
    ]
];
